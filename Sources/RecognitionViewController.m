



#import "RecognitionViewController.h"
#import "AppDelegate.h"

@implementation RecognitionViewController

@synthesize textView;
@synthesize statusLabel;
@synthesize statusIndicator;


//#error Provide Application ID and Password
// To create an application and obtain a password,
// register at http://cloud.ocrsdk.com/Account/Register
// More info on getting your application id and password at
// http://ocrsdk.com/documentation/faq/#faq3

// Name of application you created
static NSString* MyApplicationID = @"NishantCreo1XXX";
// Password should be sent to your e-mail after application was created
static NSString* MyPassword = @"2HMjkojmospt3P/sIj1JcO3tXXX";
UIActivityIndicatorView *indicator;


NSString *appPasswordUrlStr = @"http://appstore.creoinvent.co.in/ocr_creo/ocr_app_pswd.txt";
NSString *appIdUrlStr = @"http://appstore.creoinvent.co.in/ocr_creo/ocr_app_id.txt";

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

-(void)setMyApplicationID
{
    NSData *myApplicationIDData = [NSData dataWithContentsOfURL:[NSURL URLWithString: appIdUrlStr]];
    NSString *receviedAppId = [[NSString alloc] initWithData:myApplicationIDData  encoding:NSUTF8StringEncoding];
    if (receviedAppId) {
        MyApplicationID = receviedAppId;
    }
}


-(void)setMyAppPassword
{
    
    NSData *myAppPassword = [NSData dataWithContentsOfURL:[NSURL URLWithString: appPasswordUrlStr]];
    NSString *receviedAppPassword = [[NSString alloc] initWithData:myAppPassword  encoding:NSUTF8StringEncoding];
    if (receviedAppPassword) {
        MyPassword = receviedAppPassword;
    }
}

-(void)NishantInternalSetUp
{
    statusIndicator.hidden = NO;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self setMyApplicationID];
        [self setMyAppPassword];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"MyApplicationID : %@ \nMyApplicationID : %@", MyApplicationID, MyPassword);
            [self defaultProcessOfABBYY];
        });
    });
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
	[self setTextView:nil];
	[self setStatusLabel:nil];
	[self setStatusIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
	textView.hidden = YES;
	
	statusLabel.hidden = NO;
	statusIndicator. hidden = NO;
	
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
	[self NishantInternalSetUp];
}

-(void)defaultProcessOfABBYY
{
    statusLabel.text = @"Loading image...";
	
	UIImage* image = [(AppDelegate*)[[UIApplication sharedApplication] delegate] imageToProcess];
	
	Client *client = [[Client alloc] initWithApplicationID:MyApplicationID password:MyPassword];
	[client setDelegate:self];
	
	if([[NSUserDefaults standardUserDefaults] stringForKey:@"installationID"] == nil) {
		NSString* deviceID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
		
		NSLog(@"First run: obtaining installation ID..");
		NSString* installationID = [client activateNewInstallation:deviceID];
		NSLog(@"Done. Installation ID is \"%@\"", installationID);
		
		[[NSUserDefaults standardUserDefaults] setValue:installationID forKey:@"installationID"];
	}
	
	NSString* installationID = [[NSUserDefaults standardUserDefaults] stringForKey:@"installationID"];
	
	client.applicationID = [client.applicationID stringByAppendingString:installationID];
	
	ProcessingParams* params = [[ProcessingParams alloc] init];
	
	[client processImage:image withParams:params];
	
	statusLabel.text = @"Uploading image...";
	
    [super viewDidAppear: YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return NO;
}

#pragma mark - ClientDelegate implementation

- (void)clientDidFinishUpload:(Client *)sender
{
	statusLabel.text = @"Processing image...";
}

- (void)clientDidFinishProcessing:(Client *)sender
{
	statusLabel.text = @"Downloading result...";
}

- (void)client:(Client *)sender didFinishDownloadData:(NSData *)downloadedData
{
	statusLabel.hidden = YES;
	statusIndicator.hidden = YES;
	
	textView.hidden = NO;
	
	NSString* result = [[NSString alloc] initWithData:downloadedData encoding:NSUTF8StringEncoding];
	
    if (result.length<1) {
        result = @"Nothing was detected";
    }
    
	textView.text = result; 
}

- (void)client:(Client *)sender didFailedWithError:(NSError *)error
{
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:[error localizedDescription]
												   delegate:nil 
										  cancelButtonTitle:@"Cancel" 
										  otherButtonTitles:nil, nil];
	
	[alert show];
	
	statusLabel.text = [error localizedDescription];
	statusIndicator.hidden = YES;
}

@end
